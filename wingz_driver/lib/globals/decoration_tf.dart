import 'package:flutter/material.dart';
import 'package:wingz_driver/globals/colors.dart';


class DecorationTF {

  InputDecoration decorationCalTFWithPlaceholder(String placeholder, Color background, Icon icon) {
    return InputDecoration(
      hintText: placeholder,
      hintStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.w400, color: Colors.grey),
      contentPadding: EdgeInsets.all(16),
      filled: true,
      counterText: "",
      fillColor: background,
      prefixIcon: icon,
      disabledBorder:  OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(6)),
        borderSide: BorderSide(width: 0.5, color: AppColor().textFieldBgGrey),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(6)),
        borderSide: BorderSide(width: 0.5, color: AppColor().textFieldBgGrey),
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          borderSide: BorderSide(width: 0.5, color: AppColor().goldColorBg)),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          borderSide: BorderSide(width: 1, color: Colors.red)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          borderSide: BorderSide(width: 1, color: Colors.red)),
    );
  }

  InputDecoration profileDecoration() {
    return InputDecoration(
      contentPadding: EdgeInsets.all(16),
      filled: true,
      counterText: "",
      fillColor: Colors.white,
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(25)),
        borderSide: BorderSide(width: 0.5, color: Colors.white),
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          borderSide: BorderSide(width: 0.5, color: AppColor().goldColorBg)),
    );
  }

  InputDecoration supportDecoration() {
    return InputDecoration(
      contentPadding: EdgeInsets.all(16),
      filled: true,
      counterText: "",
      fillColor: Color(0xFFE8EDF6),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(25)),
        borderSide: BorderSide(width: 0.5, color: Colors.white),
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          borderSide: BorderSide(width: 0.5, color: AppColor().goldColorBg)),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          borderSide: BorderSide(width: 1, color: Colors.red)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          borderSide: BorderSide(width: 1, color: Colors.red)),
    );
  }

  TextStyle textFieldStyle(){
    return  TextStyle(fontSize: 8.0, fontWeight: FontWeight.w400, color: Colors.black);
  }

  TextStyle phoneTextFieldStyle(){
    return  TextStyle(fontSize: 10.0, fontWeight: FontWeight.w400, color: Colors.black);
  }

  InputDecoration decorationTFWithPlaceholder() {
    return InputDecoration(
      hintStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.w400, color: Colors.grey),
      contentPadding: EdgeInsets.all(12),
      filled: true,
      counterText: "",
      hintText: "Exemple : pas de tomates...",
      fillColor: Colors.black,
      disabledBorder:  OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(6)),
        borderSide: BorderSide(width: 0.5, color: AppColor().goldColorBg),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(6)),
        borderSide: BorderSide(width: 0.5, color: AppColor().textFieldBgGrey),
      ),
      focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          borderSide: BorderSide(width: 0.5, color: AppColor().goldColorBg)),
      focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          borderSide: BorderSide(width: 1, color: Colors.red)),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(6)),
          borderSide: BorderSide(width: 1, color: Colors.red)),
    );
  }
}