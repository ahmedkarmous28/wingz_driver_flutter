import 'package:email_validator/email_validator.dart';

class ValidationMixin {

  String? validateEmail(String? email) {
    if (EmailValidator.validate(email!) == false)  {
      return "Cet E-mail n'est pas valide";
    }
    return null;
  }

  String? validateNotEmpty(String? value) {

    if (value!.length == 0) {
      return "Ce champ est obligatoire";
    }
    return null;
  }

  String? validatePasswordNotEmpty(String? value) {

    if (value!.length == 0) {
      return "Ce champ est obligatoire";
    }

    if (value.length < 8) {
      return "Minimum 8 charactères.";
    }
    return null;
  }

}
