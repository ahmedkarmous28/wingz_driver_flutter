import 'package:flutter/material.dart';


class AppColor {

  Color goldColorBg = Color(0xFFaf9467);
  Color registerBtnGreen = Color(0xFF88A9BC);
  Color registerBgBlack = Color(0xFF383E42);
  Color textFieldBgGrey = Color(0xFF707070);
  Color HomeBgBlue = Color(0xFFE8EDF6);

}