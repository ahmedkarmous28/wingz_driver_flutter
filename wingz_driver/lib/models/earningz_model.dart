class EarningzModel {
  EarningzModel({
    required this.data,
    required this.status,
    required this.message,
  });
  late final Earning data;
  late final bool status;
  late final String message;

  EarningzModel.fromJson(Map<String, dynamic> json){
    data = Earning.fromJson(json['data']);
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['data'] = data.toJson();
    _data['status'] = status;
    _data['message'] = message;
    return _data;
  }
}

class Earning {
  Earning({
    required this.user,
    required this.today,
    required this.week,
    required this.month,
    required this.previous,
  });
  late final String user;
  late final Today today;
  late final Week week;
  late final Month month;
  late final Previous previous;

  Earning.fromJson(Map<String, dynamic> json){
    user = json['user'];
    today = Today.fromJson(json['today']);
    week = Week.fromJson(json['week']);
    month = Month.fromJson(json['month']);
    previous = Previous.fromJson(json['previous']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['user'] = user;
    _data['today'] = today.toJson();
    _data['week'] = week.toJson();
    _data['month'] = month.toJson();
    _data['previous'] = previous.toJson();
    return _data;
  }
}

class Today {
  Today({
    required this.orders,
    required this.earning,
  });
  late final int orders;
  late final int earning;

  Today.fromJson(Map<String, dynamic> json){
    orders = json['orders'];
    earning = json['earning'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['orders'] = orders;
    _data['earning'] = earning;
    return _data;
  }
}

class Week {
  Week({
    required this.orders,
    required this.earning,
  });
  late final int orders;
  late final int earning;

  Week.fromJson(Map<String, dynamic> json){
    orders = json['orders'];
    earning = json['earning'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['orders'] = orders;
    _data['earning'] = earning;
    return _data;
  }
}

class Month {
  Month({
    required this.orders,
    required this.earning,
  });
  late final int orders;
  late final int earning;

  Month.fromJson(Map<String, dynamic> json){
    orders = json['orders'];
    earning = json['earning'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['orders'] = orders;
    _data['earning'] = earning;
    return _data;
  }
}

class Previous {
  Previous({
    required this.orders,
    required this.earning,
  });
  late final int orders;
  late final int earning;

  Previous.fromJson(Map<String, dynamic> json){
    orders = json['orders'];
    earning = json['earning'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['orders'] = orders;
    _data['earning'] = earning;
    return _data;
  }
}