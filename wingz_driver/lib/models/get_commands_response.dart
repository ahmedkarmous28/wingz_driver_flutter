class CommandModel {
  CommandModel({
    required this.data,
    required this.driverId,
    required this.status,
    required this.message,
  });
  late final List<Command> data;
  late final int driverId;
  late final bool status;
  late final String message;

  CommandModel.fromJson(Map<String, dynamic> json){
    data = List.from(json['data']).map((e)=>Command.fromJson(e)).toList();
    driverId = json['driver_id'];
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['data'] = data.map((e)=>e.toJson()).toList();
    _data['driver_id'] = driverId;
    _data['status'] = status;
    _data['message'] = message;
    return _data;
  }
}

class Command {
  Command({
    required this.id,
    required this.createdAt,
    required this.updatedAt,
    required this.addressId,
    required this.clientId,
    required this.restorantId,
    required this.driverId,
    required this.deliveryPrice,
    required this.orderPrice,
    required this.paymentMethod,
    required this.paymentStatus,
    required this.comment,
    this.lat,
    this.lng,
    required this.srtipePaymentId,
    required this.fee,
    required this.feeValue,
    required this.staticFee,
    required this.deliveryMethod,
    required this.deliveryPickupInterval,
    required this.vatvalue,
    required this.paymentProcessorFee,
    this.timeToPrepare,
    this.tableId,
    this.phone,
    this.whatsappAddress,
    required this.paymentLink,
    required this.statut,
    required this.timeFormated,
    required this.lastStatus,
    required this.isPrepared,
    required this.actions,
    required this.items,
    required this.status,
    required this.restorant,
    required this.client,
    required this.address,
  });
  late final int id;
  late final String createdAt;
  late final String updatedAt;
  late final int addressId;
  late final int clientId;
  late final int restorantId;
  late final int? driverId;
  late final double deliveryPrice;
  late final double orderPrice;
  late final String paymentMethod;
  late final String paymentStatus;
  late final String comment;
  late final double? lat;
  late final double? lng;
  late final String? srtipePaymentId;
  late final int fee;
  late final double feeValue;
  late final int staticFee;
  late final int deliveryMethod;
  late final String deliveryPickupInterval;
  late final double vatvalue;
  late final double paymentProcessorFee;
  late final Null timeToPrepare;
  late final Null tableId;
  late final Null phone;
  late final Null whatsappAddress;
  late final String paymentLink;
  late final String statut;
  late final String timeFormated;
  late final List<LastStatus> lastStatus;
  late final bool isPrepared;
  late final Actions actions;
  late final List<Items> items;
  late final List<Status> status;
  late final Restorant restorant;
  late final Client client;
  late final Address address;

  Command.fromJson(Map<String, dynamic> json){
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    addressId = json['address_id'];
    clientId = json['client_id'];
    restorantId = json['restorant_id'];
    driverId = json['driver_id'];
    deliveryPrice = json['delivery_price'].toDouble();
    orderPrice = json['order_price'].toDouble();
    paymentMethod = json['payment_method'];
    paymentStatus = json['payment_status'];
    comment = json['comment'];
    lat = null;
    lng = null;
    srtipePaymentId = json['srtipe_payment_id'];
    fee = json['fee'];
    feeValue = json['fee_value'].toDouble();
    staticFee = json['static_fee'];
    deliveryMethod = json['delivery_method'];
    deliveryPickupInterval = json['delivery_pickup_interval'];
    vatvalue = json['vatvalue'].toDouble();
    paymentProcessorFee = json['payment_processor_fee'].toDouble();
    timeToPrepare = null;
    tableId = null;
    phone = null;
    whatsappAddress = null;
    paymentLink = json['payment_link'];
    statut = json['statut'];
    timeFormated = json['time_formated'];
    lastStatus = List.from(json['last_status']).map((e)=>LastStatus.fromJson(e)).toList();
    isPrepared = json['is_prepared'];
    actions = Actions.fromJson(json['actions']);
    items = List.from(json['items']).map((e)=>Items.fromJson(e)).toList();
    status = List.from(json['status']).map((e)=>Status.fromJson(e)).toList();
    restorant = Restorant.fromJson(json['restorant']);
    client = Client.fromJson(json['client']);
    address = Address.fromJson(json['address']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    _data['address_id'] = addressId;
    _data['client_id'] = clientId;
    _data['restorant_id'] = restorantId;
    _data['driver_id'] = driverId;
    _data['delivery_price'] = deliveryPrice;
    _data['order_price'] = orderPrice;
    _data['payment_method'] = paymentMethod;
    _data['payment_status'] = paymentStatus;
    _data['comment'] = comment;
    _data['lat'] = lat;
    _data['lng'] = lng;
    _data['srtipe_payment_id'] = srtipePaymentId;
    _data['fee'] = fee;
    _data['fee_value'] = feeValue;
    _data['static_fee'] = staticFee;
    _data['delivery_method'] = deliveryMethod;
    _data['delivery_pickup_interval'] = deliveryPickupInterval;
    _data['vatvalue'] = vatvalue;
    _data['payment_processor_fee'] = paymentProcessorFee;
    _data['time_to_prepare'] = timeToPrepare;
    _data['table_id'] = tableId;
    _data['phone'] = phone;
    _data['whatsapp_address'] = whatsappAddress;
    _data['payment_link'] = paymentLink;
    _data['statut'] = statut;
    _data['time_formated'] = timeFormated;
    _data['last_status'] = lastStatus.map((e)=>e.toJson()).toList();
    _data['is_prepared'] = isPrepared;
    _data['actions'] = actions.toJson();
    _data['items'] = items.map((e)=>e.toJson()).toList();
    _data['status'] = status.map((e)=>e.toJson()).toList();
    _data['restorant'] = restorant.toJson();
    _data['client'] = client.toJson();
    _data['address'] = address.toJson();
    return _data;
  }
}

class LastStatus {
  LastStatus({
    required this.id,
    required this.name,
    required this.alias,
    required this.pivot,
  });
  late final int id;
  late final String name;
  late final String alias;
  late final Pivot pivot;

  LastStatus.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    alias = json['alias'];
    pivot = Pivot.fromJson(json['pivot']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['alias'] = alias;
    _data['pivot'] = pivot.toJson();
    return _data;
  }
}

class Pivot {
  Pivot({
    required this.orderId,
    required this.statusId,
    required this.userId,
    required this.createdAt,
    required this.comment,
  });
  late final int orderId;
  late final int? statusId;
  late final int? userId;
  late final String? createdAt;
  late final String? comment;

  Pivot.fromJson(Map<String, dynamic> json){
    orderId = json['order_id'];
    statusId = json['status_id'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    comment = json['comment'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['order_id'] = orderId;
    _data['status_id'] = statusId;
    _data['user_id'] = userId;
    _data['created_at'] = createdAt;
    _data['comment'] = comment;
    return _data;
  }
}

class Actions {
  Actions({
    required this.buttons,
    required this.message,
  });
  late final List<dynamic> buttons;
  late final String message;

  Actions.fromJson(Map<String, dynamic> json){
    buttons = List.castFrom<dynamic, dynamic>(json['buttons']);
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['buttons'] = buttons;
    _data['message'] = message;
    return _data;
  }
}

class Items {
  Items({
    required this.id,
    required this.name,
    required this.description,
    required this.image,
    required this.price,
    required this.categoryId,
    required this.createdAt,
    required this.updatedAt,
    required this.available,
    required this.hasVariants,
    this.vat,
    this.deletedAt,
    required this.logom,
    required this.icon,
    required this.shortDescription,
    required this.pivot,
  });
  late final int id;
  late final String name;
  late final String description;
  late final String image;
  late final double price;
  late final int categoryId;
  late final String createdAt;
  late final String updatedAt;
  late final int available;
  late final int hasVariants;
  late final Null vat;
  late final Null deletedAt;
  late final String logom;
  late final String icon;
  late final String shortDescription;
  late final PivotItem pivot;

  Items.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    description = json['description'];
    image = json['image'];
    price = json['price'].toDouble();
    categoryId = json['category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    available = json['available'];
    hasVariants = json['has_variants'];
    vat = null;
    deletedAt = null;
    logom = json['logom'];
    icon = json['icon'];
    shortDescription = json['short_description'];
    pivot = PivotItem.fromJson(json['pivot']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['description'] = description;
    _data['image'] = image;
    _data['price'] = price;
    _data['category_id'] = categoryId;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    _data['available'] = available;
    _data['has_variants'] = hasVariants;
    _data['vat'] = vat;
    _data['deleted_at'] = deletedAt;
    _data['logom'] = logom;
    _data['icon'] = icon;
    _data['short_description'] = shortDescription;
    _data['pivot'] = pivot.toJson();
    return _data;
  }
}

class PivotItem {
  PivotItem({
    required this.orderId,
    required this.itemId,
    required this.qty,
    required this.extras,
    this.vat,
    required this.vatvalue,
    required this.variantPrice,
    required this.variantName,
    this.comment,
  });
  late final int orderId;
  late final int itemId;
  late final int qty;
  late final String extras;
  late final Null vat;
  late final double vatvalue;
  late final double variantPrice;
  late final String variantName;
  late final Null comment;

  PivotItem.fromJson(Map<String, dynamic> json){
    orderId = json['order_id'];
    itemId = json['item_id'];
    qty = json['qty'];
    extras = json['extras'];
    vat = null;
    vatvalue = json['vatvalue'].toDouble();;
    variantPrice = json['variant_price'].toDouble();
    variantName = json['variant_name'];
    comment = null;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['order_id'] = orderId;
    _data['item_id'] = itemId;
    _data['qty'] = qty;
    _data['extras'] = extras;
    _data['vat'] = vat;
    _data['vatvalue'] = vatvalue;
    _data['variant_price'] = variantPrice;
    _data['variant_name'] = variantName;
    _data['comment'] = comment;
    return _data;
  }
}

class Status {
  Status({
    required this.id,
    required this.name,
    required this.alias,
    required this.pivot,
  });
  late final int id;
  late final String name;
  late final String alias;
  late final Pivot pivot;

  Status.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    alias = json['alias'];
    pivot = Pivot.fromJson(json['pivot']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['name'] = name;
    _data['alias'] = alias;
    _data['pivot'] = pivot.toJson();
    return _data;
  }
}

class Restorant {
  Restorant({
    required this.id,
    required this.createdAt,
    required this.updatedAt,
    required this.name,
    required this.subdomain,
    required this.logo,
    required this.cover,
    required this.active,
    required this.userId,
    required this.lat,
    required this.lng,
    required this.address,
    required this.phone,
    required this.minimum,
    required this.description,
    required this.fee,
    required this.staticFee,
    required this.radius,
    required this.isFeatured,
    required this.cityId,
    required this.views,
    required this.canPickup,
    required this.canDeliver,
    required this.selfDeliver,
    required this.freeDeliver,
    this.whatsappPhone,
    this.fbUsername,
    required this.doCovertion,
    required this.currency,
    required this.paymentInfo,
    required this.molliePaymentKey,
    required this.alias,
    required this.logom,
    required this.icon,
    required this.coverm,
  });
  late final int id;
  late final String createdAt;
  late final String updatedAt;
  late final String name;
  late final String subdomain;
  late final String logo;
  late final String cover;
  late final int active;
  late final int userId;
  late final String lat;
  late final String lng;
  late final String address;
  late final String phone;
  late final String minimum;
  late final String description;
  late final int fee;
  late final int staticFee;
  late final List<dynamic> radius;
  late final int isFeatured;
  late final int cityId;
  late final int views;
  late final int canPickup;
  late final int canDeliver;
  late final int selfDeliver;
  late final int freeDeliver;
  late final Null whatsappPhone;
  late final Null fbUsername;
  late final int doCovertion;
  late final String currency;
  late final String paymentInfo;
  late final String molliePaymentKey;
  late final String alias;
  late final String logom;
  late final String icon;
  late final String coverm;

  Restorant.fromJson(Map<String, dynamic> json){
    id = json['id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    name = json['name'];
    subdomain = json['subdomain'];
    logo = json['logo'];
    cover = json['cover'];
    active = json['active'];
    userId = json['user_id'];
    lat = json['lat'];
    lng = json['lng'];
    address = json['address'];
    phone = json['phone'];
    minimum = json['minimum'];
    description = json['description'];
    fee = json['fee'];
    staticFee = json['static_fee'];
    radius = List.castFrom<dynamic, dynamic>(json['radius']);
    isFeatured = json['is_featured'];
    cityId = json['city_id'];
    views = json['views'];
    canPickup = json['can_pickup'];
    canDeliver = json['can_deliver'];
    selfDeliver = json['self_deliver'];
    freeDeliver = json['free_deliver'];
    whatsappPhone = null;
    fbUsername = null;
    doCovertion = json['do_covertion'];
    currency = json['currency'];
    paymentInfo = json['payment_info'];
    molliePaymentKey = json['mollie_payment_key'];
    alias = json['alias'];
    logom = json['logom'];
    icon = json['icon'];
    coverm = json['coverm'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    _data['name'] = name;
    _data['subdomain'] = subdomain;
    _data['logo'] = logo;
    _data['cover'] = cover;
    _data['active'] = active;
    _data['user_id'] = userId;
    _data['lat'] = lat;
    _data['lng'] = lng;
    _data['address'] = address;
    _data['phone'] = phone;
    _data['minimum'] = minimum;
    _data['description'] = description;
    _data['fee'] = fee;
    _data['static_fee'] = staticFee;
    _data['radius'] = radius;
    _data['is_featured'] = isFeatured;
    _data['city_id'] = cityId;
    _data['views'] = views;
    _data['can_pickup'] = canPickup;
    _data['can_deliver'] = canDeliver;
    _data['self_deliver'] = selfDeliver;
    _data['free_deliver'] = freeDeliver;
    _data['whatsapp_phone'] = whatsappPhone;
    _data['fb_username'] = fbUsername;
    _data['do_covertion'] = doCovertion;
    _data['currency'] = currency;
    _data['payment_info'] = paymentInfo;
    _data['mollie_payment_key'] = molliePaymentKey;
    _data['alias'] = alias;
    _data['logom'] = logom;
    _data['icon'] = icon;
    _data['coverm'] = coverm;
    return _data;
  }
}

class Client {
  Client({
    required this.id,
    this.googleId,
    this.fbId,
    required this.name,
    required this.email,
    this.emailVerifiedAt,
    required this.phone,
    required this.createdAt,
    required this.updatedAt,
    required this.active,
    this.stripeId,
    this.cardBrand,
    this.cardLastFour,
    this.trialEndsAt,
    this.verificationCode,
    this.phoneVerifiedAt,
    this.planId,
    required this.planStatus,
    required this.cancelUrl,
    required this.updateUrl,
    required this.checkoutId,
    required this.subscriptionPlanId,
    required this.stripeAccount,
    required this.birthDate,
    this.lat,
    this.lng,
    required this.working,
    this.onorder,
    required this.numorders,
    required this.rejectedorders,
    this.paypalSubscribtionId,
    this.mollieCustomerId,
    this.mollieMandateId,
    required this.taxPercentage,
    this.extraBillingInformation,
    this.mollieSubscribtionId,
    this.paystackSubscribtionId,
    this.paystackTransId,
    required this.rib,
    required this.address,
    required this.kbis,
    required this.permis,
    required this.assurance,
  });
  late final int id;
  late final Null googleId;
  late final Null fbId;
  late final String name;
  late final String email;
  late final Null emailVerifiedAt;
  late final String phone;
  late final String createdAt;
  late final String updatedAt;
  late final int active;
  late final Null stripeId;
  late final Null cardBrand;
  late final Null cardLastFour;
  late final Null trialEndsAt;
  late final Null verificationCode;
  late final Null phoneVerifiedAt;
  late final Null planId;
  late final String planStatus;
  late final String cancelUrl;
  late final String updateUrl;
  late final String checkoutId;
  late final String subscriptionPlanId;
  late final String stripeAccount;
  late final String birthDate;
  late final Null lat;
  late final Null lng;
  late final int working;
  late final Null onorder;
  late final int numorders;
  late final int rejectedorders;
  late final Null paypalSubscribtionId;
  late final Null mollieCustomerId;
  late final Null mollieMandateId;
  late final String taxPercentage;
  late final Null extraBillingInformation;
  late final Null mollieSubscribtionId;
  late final Null paystackSubscribtionId;
  late final Null paystackTransId;
  late final String rib;
  late final String address;
  late final String kbis;
  late final String permis;
  late final String assurance;

  Client.fromJson(Map<String, dynamic> json){
    id = json['id'];
    googleId = null;
    fbId = null;
    name = json['name'];
    email = json['email'];
    emailVerifiedAt = null;
    phone = json['phone'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    active = json['active'];
    stripeId = null;
    cardBrand = null;
    cardLastFour = null;
    trialEndsAt = null;
    verificationCode = null;
    phoneVerifiedAt = null;
    planId = null;
    planStatus = json['plan_status'];
    cancelUrl = json['cancel_url'];
    updateUrl = json['update_url'];
    checkoutId = json['checkout_id'];
    subscriptionPlanId = json['subscription_plan_id'];
    stripeAccount = json['stripe_account'];
    birthDate = json['birth_date'];
    lat = null;
    lng = null;
    working = json['working'];
    onorder = null;
    numorders = json['numorders'];
    rejectedorders = json['rejectedorders'];
    paypalSubscribtionId = null;
    mollieCustomerId = null;
    mollieMandateId = null;
    taxPercentage = json['tax_percentage'];
    extraBillingInformation = null;
    mollieSubscribtionId = null;
    paystackSubscribtionId = null;
    paystackTransId = null;
    rib = json['rib'];
    address = json['address'];
    kbis = json['kbis'];
    permis = json['permis'];
    assurance = json['assurance'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['google_id'] = googleId;
    _data['fb_id'] = fbId;
    _data['name'] = name;
    _data['email'] = email;
    _data['email_verified_at'] = emailVerifiedAt;
    _data['phone'] = phone;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    _data['active'] = active;
    _data['stripe_id'] = stripeId;
    _data['card_brand'] = cardBrand;
    _data['card_last_four'] = cardLastFour;
    _data['trial_ends_at'] = trialEndsAt;
    _data['verification_code'] = verificationCode;
    _data['phone_verified_at'] = phoneVerifiedAt;
    _data['plan_id'] = planId;
    _data['plan_status'] = planStatus;
    _data['cancel_url'] = cancelUrl;
    _data['update_url'] = updateUrl;
    _data['checkout_id'] = checkoutId;
    _data['subscription_plan_id'] = subscriptionPlanId;
    _data['stripe_account'] = stripeAccount;
    _data['birth_date'] = birthDate;
    _data['lat'] = lat;
    _data['lng'] = lng;
    _data['working'] = working;
    _data['onorder'] = onorder;
    _data['numorders'] = numorders;
    _data['rejectedorders'] = rejectedorders;
    _data['paypal_subscribtion_id'] = paypalSubscribtionId;
    _data['mollie_customer_id'] = mollieCustomerId;
    _data['mollie_mandate_id'] = mollieMandateId;
    _data['tax_percentage'] = taxPercentage;
    _data['extra_billing_information'] = extraBillingInformation;
    _data['mollie_subscribtion_id'] = mollieSubscribtionId;
    _data['paystack_subscribtion_id'] = paystackSubscribtionId;
    _data['paystack_trans_id'] = paystackTransId;
    _data['rib'] = rib;
    _data['address'] = address;
    _data['kbis'] = kbis;
    _data['permis'] = permis;
    _data['assurance'] = assurance;
    return _data;
  }
}

class Address {
  Address({
    required this.id,
    required this.address,
    required this.createdAt,
    required this.updatedAt,
    required this.lat,
    required this.lng,
    required this.active,
    required this.userId,
    this.apartment,
    this.intercom,
    this.floor,
    this.entry,
  });
  late final int id;
  late final String address;
  late final String createdAt;
  late final String updatedAt;
  late final String lat;
  late final String lng;
  late final int active;
  late final int userId;
  late final Null apartment;
  late final Null intercom;
  late final Null floor;
  late final Null entry;

  Address.fromJson(Map<String, dynamic> json){
    id = json['id'];
    address = json['address'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    lat = json['lat'];
    lng = json['lng'];
    active = json['active'];
    userId = json['user_id'];
    apartment = null;
    intercom = null;
    floor = null;
    entry = null;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['address'] = address;
    _data['created_at'] = createdAt;
    _data['updated_at'] = updatedAt;
    _data['lat'] = lat;
    _data['lng'] = lng;
    _data['active'] = active;
    _data['user_id'] = userId;
    _data['apartment'] = apartment;
    _data['intercom'] = intercom;
    _data['floor'] = floor;
    _data['entry'] = entry;
    return _data;
  }
}