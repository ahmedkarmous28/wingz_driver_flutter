class RegisterResponse {
  bool? status;
  String? token;
  int? id;

  RegisterResponse({this.status, this.token, this.id});

  RegisterResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    token = json['token'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['token'] = this.token;
    data['id'] = this.id;
    return data;
  }
}
