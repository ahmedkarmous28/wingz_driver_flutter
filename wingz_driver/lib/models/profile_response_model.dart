class ProfileModel {
  ProfileModel({
    required this.status,
    required this.data,
    required this.msg,
  });
  late final bool status;
  late final ProfileData data;
  late final String msg;

  ProfileModel.fromJson(Map<String, dynamic> json){
    status = json['status'];
    data = ProfileData.fromJson(json['data']);
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['status'] = status;
    _data['data'] = data.toJson();
    _data['msg'] = msg;
    return _data;
  }
}

class ProfileData {
  ProfileData({
    required this.name,
    required this.email,
    required this.phone,
  });
  late final String name;
  late final String email;
  late final String phone;

  ProfileData.fromJson(Map<String, dynamic> json){
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['email'] = email;
    _data['phone'] = phone;
    return _data;
  }
}