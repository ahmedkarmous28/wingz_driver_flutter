
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wingz_driver/globals/constants.dart';

class SharedPrefsService {

  Future<String> getName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final name = prefs.getString(NAME);
    return "$name";
  }

  Future<String> getEmail() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final email = prefs.getString(EMAIL);
    return "$email";
  }

  Future<List<String>?> getBasket() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final basket = prefs.getStringList(BASKET);
    return basket;
  }

}
