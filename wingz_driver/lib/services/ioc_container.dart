// This is where all your instances and factories are stored
// ignore: import_of_legacy_library_into_null_safe
import 'package:kiwi/kiwi.dart';

import 'package:dio/dio.dart';
import 'package:event_bus/event_bus.dart';
import 'package:wingz_driver/services/date_service.dart';
import 'package:wingz_driver/services/location_service.dart';
import 'package:wingz_driver/services/shared_pref_service.dart';
import 'token_service.dart';
import 'http_service.dart';
import 'alert_service.dart';

KiwiContainer container = KiwiContainer();

/// register IoC instances
void setupIocContainer() {
  container.registerFactory((KiwiContainer container) => Dio());
  container.registerSingleton((KiwiContainer container) => EventBus());
  container.registerSingleton((KiwiContainer container) => AlertService());
  container.registerSingleton((KiwiContainer container) => LocationService());
  container.registerSingleton((KiwiContainer container) => SharedPrefsService());
  container.registerFactory((KiwiContainer container) => TokenService());
  container.registerSingleton((KiwiContainer container) => DateService());
  container.registerSingleton((KiwiContainer container) => HttpService(container.resolve<Dio>(), container.resolve<TokenService>()));
}
