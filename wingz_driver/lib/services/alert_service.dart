import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:wingz_driver/globals/colors.dart';


class AlertService{

  void showErrorAlert(BuildContext context,  String message){
    Alert(
      context: context,
      title: "Erreur",
      style: AlertStyle(
          descStyle: TextStyle(
              fontSize: 14
          ),
          titleStyle: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold
          ),
          isCloseButton: true,
          isButtonVisible: false
      ),
      desc: message,
    ).show();
  }

  void showSuccessAlert(BuildContext context,  String message){
    Alert(
      context: context,
      title: "Succès",
      image: Icon(Icons.check_circle_outline, size: 40,color: Colors.green),
      style: AlertStyle(
          descStyle: TextStyle(
              fontSize: 14
          ),
          titleStyle: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold
          ),
          isCloseButton: true,
          isButtonVisible: false
      ),
      desc: message,
    ).show();
  }

  void showCommandSuccessAlert(BuildContext context,  String message, Function action){
    Alert(
      context: context,
      title: "Succès",
      image: Icon(Icons.check_circle_outline, size: 40,color: Colors.green),
      style: AlertStyle(
          descStyle: TextStyle(
              fontSize: 14
          ),
          titleStyle: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold
          ),
          isCloseButton: true,
          isButtonVisible: true
      ),
      buttons: [
        DialogButton(
          color: AppColor().goldColorBg,
          child: Text(
            "Fermer",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => action(),
          width: 120,
        )
      ],
      desc: message,
    ).show();
  }

  void showCancelCommand(BuildContext context,  String message, Function action){
    Alert(
      context: context,
      title: "Refuser la commande",
      style: AlertStyle(
          descStyle: TextStyle(
              fontSize: 12
          ),
          titleStyle: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold
          ),
          isCloseButton: true,
          isButtonVisible: true
      ),
      desc: message,
      image: Image.asset("assets/images/icons8-Color-63652-100-ffffff.png"),
      buttons: [
        DialogButton(
          color: AppColor().goldColorBg,
          child: Text(
            "Valider",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => action(),
          width: 120,
        ),
        DialogButton(
          color: AppColor().goldColorBg,
          child: Text(
            "Fermer",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.of(context).pop(),
          width: 120,
        )
      ],
    ).show();
  }

  void showAcceptCommand(BuildContext context,  String message, Function action){
    Alert(
      context: context,
      title: "Accepter la commande",
      style: AlertStyle(
          descStyle: TextStyle(
              fontSize: 12
          ),
          titleStyle: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.bold
          ),
          isCloseButton: true,
          isButtonVisible: true
      ),
      desc: message,
      image: Image.asset("assets/images/icons8-Emoji-sz8cPVwzLrMP-100-ffffff.png"),
      buttons: [
        DialogButton(
          color: AppColor().goldColorBg,
          child: Text(
            "Valider",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => action(),

          width: 120,
        ),
        DialogButton(
          color: AppColor().goldColorBg,
          child: Text(
            "Fermer",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.of(context).pop(),
          width: 120,
        )
      ],
    ).show();
  }
}