import "package:dio/dio.dart";
import 'package:wingz_driver/bloc/api_settings.dart';
import 'token_service.dart';

class HttpService {
  final Dio _dio;
  final TokenService _tokenService;

  HttpService(
      this._dio,
      this._tokenService,
      ) {
    _setInitialSettings();
    _addTokenInterceptor();
  }

  void _setInitialSettings() {
    _dio.options.baseUrl = BASE_URL;
    _dio.options.contentType = Headers.jsonContentType;
    _dio.options.connectTimeout = TIMEOUT_DURATION;
    _dio.options.receiveTimeout = TIMEOUT_DURATION;
    _dio.options.sendTimeout = TIMEOUT_DURATION;
  }

  void _addTokenInterceptor() {
    _dio.interceptors.add(InterceptorsWrapper(
        onRequest:(options, handler){
          // Do something before request is sent
          _tokenService.getAccessToken().then((authInfo) {
            options.headers["authorization"] = "Bearer $authInfo";
            handler.next(options);
          });
        },
        onResponse:(response,handler) {
          // Do something with response data
          return handler.next(response); // continue
        },
        onError: (DioError e, handler) {
          // Do something with response error
          return  handler.next(e);//continue
        }
    ));

  }

  Future<Response<dynamic>> doGet(String uri, {Options? options, String? baseUrl}) async {
    Response<dynamic> response;

    if (baseUrl != null) {
      _dio.options.baseUrl = baseUrl;
    }

    response = await _dio.get(uri, options: options).catchError((error) => null);

    _setInitialSettings();

    return response;
  }

  Future<Response<dynamic>> doPost(String uri, dynamic data, {Options? options}) async {
    print(uri);
    final Response<dynamic> response = await _dio.post(uri, data: data, options: options).catchError((error) => print(error));
    return response;
  }

  Future<Response<dynamic>> doPatch(String uri, dynamic data, {Options? options}) async {
    final Response<dynamic> response = await _dio.patch(uri, data: data, options: options).catchError((error) => null);

    return response;
  }

  Future<Response<dynamic>> doPut(String uri, dynamic data, {Options? options}) async {
    final Response<dynamic> response = await _dio.put(uri, data: data, options: options).catchError((error) => null);

    return response;
  }
}
