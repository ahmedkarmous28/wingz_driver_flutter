
const BASE_URL = "https://wingz.delivery/api/v2";
const TIMEOUT_DURATION = 30000;
const IMAGE_URL = "https://wingz.delivery/";


const LOGIN_URL = "/driver/auth/gettoken";
const REGISTER_URL = "/driver/auth/register";

const UPDATE_URL = "/driver/auth/updateMobileProfile";
const UPDATE_PASSWORD_URL = "/driver/auth/changePassword";

const AUTH_DATA_URL = "/driver/auth/data";
const NOTIFICATIONS_URL = "/client/notifications";

const OFFLINE_URL = "/driver/auth/drveroffline";
const ONLINE_URL = "/driver/auth/driveronline";

const MY_COMMANDS_URL = "/driver/orders/ownOrders";
const COMMAND_URL = "/driver/orders";

const EARNINGS_URL = "/driver/orders/earnings";

const UPDATE_ORDER_STATUS_URL = "/driver/orders/updateorderstatus/";
const ORDER_DETAILS_URL = "/driver/orders/order/";
const UPDATE_LOCATION_URL = "/updateorderlocation/";

