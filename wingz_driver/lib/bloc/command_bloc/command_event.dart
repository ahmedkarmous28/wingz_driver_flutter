part of 'command_bloc.dart';

abstract class CommandEvent extends Equatable {
  const CommandEvent();
}

class GetNotifications extends CommandEvent {

  @override
  List<Object> get props => [];
}

class GetMyOrders extends CommandEvent {

  @override
  List<Object> get props => [];
}

class GetOrders extends CommandEvent {

  @override
  List<Object> get props => [];
}

class AcceptCommand extends CommandEvent {

  final int id;
  AcceptCommand(this.id);

  @override
  List<Object> get props => [];
}

class RejectCommand extends CommandEvent {

  final int id;
  RejectCommand(this.id);

  @override
  List<Object> get props => [];
}


class UpdateStatusCommand extends CommandEvent {

  final int idStatus, id;
  UpdateStatusCommand(this.id, this.idStatus);

  @override
  List<Object> get props => [];
}


class SetAvailable extends CommandEvent {

  @override
  List<Object> get props => [];
}

class SetUnavailable extends CommandEvent {

  @override
  List<Object> get props => [];
}

class GetEarnings extends CommandEvent {

  @override
  List<Object> get props => [];
}