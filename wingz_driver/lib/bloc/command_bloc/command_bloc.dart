import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:wingz_driver/models/earningz_model.dart';
import 'package:wingz_driver/models/get_commands_response.dart';
import 'package:wingz_driver/models/get_notifications_model.dart';
import 'package:wingz_driver/services/http_service.dart';
import 'package:wingz_driver/services/ioc_container.dart';
import 'package:wingz_driver/services/location_service.dart';
import 'package:wingz_driver/services/token_service.dart';

import '../api_settings.dart';

part 'command_event.dart';
part 'command_state.dart';

class CommandBloc extends Bloc<CommandEvent, CommandState> {
  CommandBloc() : super(CommandInitial());

  var _dio = Dio();
  var _httpService  = container.resolve<HttpService>();

  @override
  Stream<CommandState> mapEventToState(
    CommandEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is GetNotifications) {
      try {
        print("Calling Get notifications");
        yield CommandInitial();

        var _tokenService  = container.resolve<TokenService>();
        final token = await _tokenService.getAccessToken();

        var url = "$NOTIFICATIONS_URL?api_token=$token";

        final Response<dynamic> response = await _httpService.doGet(url);
        print(response);

        if (response.statusCode == 200) {
          NotificationModel notificationsData = NotificationModel.fromJson(response.data);
          yield SuccessNotificationsState(notificationsData.data);
        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error login user");
      }
    }else if (event is GetOrders) {
      //try {
        print("Calling Get orders");
        yield CommandInitial();

        var _tokenService  = container.resolve<TokenService>();
        final token = await _tokenService.getAccessToken();

        var url = "$COMMAND_URL?api_token=$token";

        final Response<dynamic> response = await _httpService.doGet(url);
        print(response);

        if (response.statusCode == 200) {
          CommandModel commandModel = CommandModel.fromJson(response.data);
          yield SuccessOrdersState(commandModel.data);
        }
        //else {
          //yield ErrorState('error');
        //}
       // } catch (e) {
        //  yield ErrorState("error Get orders");
     // }
    }else if (event is GetMyOrders) {
      try {
        print("Calling Get my orders");
        yield CommandInitial();

        var _tokenService  = container.resolve<TokenService>();
        final token = await _tokenService.getAccessToken();

        var url = "$MY_COMMANDS_URL?api_token=$token";

        final Response<dynamic> response = await _httpService.doGet(url);
        print(response);

        if (response.statusCode == 200) {
          CommandModel commandModel = CommandModel.fromJson(response.data);
          yield SuccessMyOrdersState(commandModel.data);

          if(commandModel.data.length > 0){
            var _locationService  = container.resolve<LocationService>();

            _locationService.determinePosition().then((location) async {
              print(location.latitude);
              print(location.longitude);

              var url = "$UPDATE_LOCATION_URL/${commandModel.data.first.id}/${location.latitude}/${location.longitude}?api_token=$token";
              final Response<dynamic> locationResponse = await _httpService.doGet(url);
              print(locationResponse);
            });
          }

        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error login user");
      }
    }else if (event is AcceptCommand) {
      try {
      print("Calling AcceptCommand");
      yield CommandInitial();

      var _tokenService  = container.resolve<TokenService>();
      final token = await _tokenService.getAccessToken();

      var url = "$UPDATE_ORDER_STATUS_URL${event.id}/13?api_token=$token";

      final Response<dynamic> response = await _httpService.doGet(url);
      print(response);

      if (response.statusCode == 200) {
        yield SuccessAcceptCommandState(true);
      } else {
        yield ErrorState('error');
      }
      } catch (e) {
        yield ErrorState("error login user");
      }
    }else if (event is RejectCommand) {
      try {
      print("Calling RejectCommand");
      yield CommandInitial();

      var _tokenService  = container.resolve<TokenService>();
      final token = await _tokenService.getAccessToken();

      var url = "$UPDATE_ORDER_STATUS_URL${event.id}/12?api_token=$token";

      final Response<dynamic> response = await _httpService.doGet(url);
      print(response);

      if (response.statusCode == 200) {
        yield SuccessRefuseCommandState(true);
      } else {
        yield ErrorState('error');
      }
      } catch (e) {
        yield ErrorState("error login user");
      }
    }else if (event is SetAvailable) {
      try {
        print("Calling SetAvailable");
        yield CommandInitial();

        var _tokenService  = container.resolve<TokenService>();
        final token = await _tokenService.getAccessToken();

        var url = "$ONLINE_URL?api_token=$token";

        final Response<dynamic> response = await _httpService.doGet(url);
        print(response);

        if (response.statusCode == 200) {
          yield UpdateDriverStatusState(true);
        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error login user");
      }
    } else if (event is SetUnavailable) {
      try {
        print("Calling SetUnavailable");
        yield CommandInitial();

        var _tokenService  = container.resolve<TokenService>();
        final token = await _tokenService.getAccessToken();

        var url = "$OFFLINE_URL?api_token=$token";

        final Response<dynamic> response = await _httpService.doGet(url);
        print(response);

        if (response.statusCode == 200) {
          yield UpdateDriverUnavailableStatusState(true);
        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error login user");
      }
    }else if (event is GetEarnings) {
      try {
        print("Calling GetEarnings");
        yield CommandInitial();

        var _tokenService  = container.resolve<TokenService>();
        final token = await _tokenService.getAccessToken();

        var url = "$EARNINGS_URL?api_token=$token";
        print(url);
        final Response<dynamic> response = await _httpService.doGet(url);
        print(response);

        if (response.statusCode == 200) {
          EarningzModel earningModel = EarningzModel.fromJson(response.data);
          yield SuccessEarningsState(earningModel.data);
        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error login user");
      }
    }else if (event is UpdateStatusCommand) {
      try {
        print("Calling UpdateStatusCommand");
        yield CommandInitial();

        var _tokenService  = container.resolve<TokenService>();
        final token = await _tokenService.getAccessToken();

        var url = "$UPDATE_ORDER_STATUS_URL${event.id}/${event.idStatus}?api_token=$token";
        print(url);

        final Response<dynamic> response = await _httpService.doGet(url);
        print(response);

        if (response.statusCode == 200) {
          yield SuccessUpdateCommand(true);
        } else {
          yield ErrorState('error');
        }
      } catch (e) {
        yield ErrorState("error login user");
      }
    }
  }
}
