part of 'command_bloc.dart';

abstract class CommandState extends Equatable {
  const CommandState();
}

class CommandInitial extends CommandState {
  @override
  List<Object> get props => [];
}

class ErrorState extends CommandState {
  final String error;

  ErrorState(this.error);
  @override
  List<Object> get props => [error];
}

class SuccessNotificationsState extends CommandState {
  final List<NotificationUser> response;

  SuccessNotificationsState(this.response);
  @override
  List<Object> get props => [response];
}

class SuccessMyOrdersState extends CommandState {
  final List<Command> response;

  SuccessMyOrdersState(this.response);
  @override
  List<Object> get props => [response];
}

class SuccessOrdersState extends CommandState {
  final List<Command> response;

  SuccessOrdersState(this.response);
  @override
  List<Object> get props => [response];
}

class SuccessAcceptCommandState extends CommandState {
  final bool response;

  SuccessAcceptCommandState(this.response);
  @override
  List<Object> get props => [response];
}

class SuccessRefuseCommandState extends CommandState {
  final bool response;

  SuccessRefuseCommandState(this.response);
  @override
  List<Object> get props => [response];
}

class UpdateDriverStatusState extends CommandState {
  final bool response;

  UpdateDriverStatusState(this.response);
  @override
  List<Object> get props => [response];
}

class UpdateDriverUnavailableStatusState extends CommandState {
  final bool response;

  UpdateDriverUnavailableStatusState(this.response);
  @override
  List<Object> get props => [response];
}


class SuccessEarningsState extends CommandState {
  final Earning response;

  SuccessEarningsState(this.response);
  @override
  List<Object> get props => [response];
}

class SuccessUpdateCommand extends CommandState {
  final bool response;

  SuccessUpdateCommand(this.response);
  @override
  List<Object> get props => [response];
}


