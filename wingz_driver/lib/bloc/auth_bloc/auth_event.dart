part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class LoginUser extends AuthEvent {
  final String email, password;

  LoginUser(this.email, this.password);

  @override
  List<Object> get props => [email, password];
}

class RegisterUser extends AuthEvent {
  final Map<String, String> params;

  RegisterUser(this.params);

  @override
  List<Object> get props => [params];
}

class AddAddress extends AuthEvent {
  final Map<String, String?> params;

  AddAddress(this.params);

  @override
  List<Object> get props => [params];
}

class UpdateUser extends AuthEvent {
  final Map<String, String> params;

  UpdateUser(this.params);

  @override
  List<Object> get props => [params];
}

class UpdateUserPassword extends AuthEvent {
  final Map<String, String> params;

  UpdateUserPassword(this.params);

  @override
  List<Object> get props => [params];
}