import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wingz_driver/bloc/auth_bloc/auth_bloc.dart';
import 'package:wingz_driver/globals/colors.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/globals/decoration_tf.dart';
import 'package:wingz_driver/globals/mixins.dart';
import 'package:wingz_driver/screens/commands_screen/commands_screen.dart';
import 'package:wingz_driver/screens/register_screen/register_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wingz_driver/services/alert_service.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen>  with ValidationMixin {

  final _formKey = GlobalKey<FormState>();
  final _authBloc = AuthBloc();
  bool isLoading = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void dispose() {
    _authBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:  BlocListener(
          bloc: _authBloc,
          listener: (c, AuthState state) async {
            if (state is AuthInitial) {
              print("-------state-------");
              print(state);
              EasyLoading.show(status: 'WINGZ');
            }

            if (state is LoginUserState) {
              print("Auth success ${state.response.token}");
              EasyLoading.dismiss();

              if(state.response.token == null){
                AlertService().showErrorAlert(context, 'Veuillez vérifier vos coordonnées');
                return;
              }

              final SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.setString(TOKEN, state.response.token!);
              prefs.setString(USER_ID, state.response.id.toString());
              prefs.setString(NAME, state.response.name ?? "N/A");
              prefs.setString(EMAIL, state.response.email ?? "N/A");
              prefs.setString(PHONE, state.response.phone ?? "N/A");
              prefs.setBool(CONNECTED, true);

              Future.delayed(Duration(milliseconds: 1500), () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => CommandTabScreen(fromLogin: true)));
              });
            }

            if (state is ErrorState ) {
              EasyLoading.dismiss();
              AlertService().showErrorAlert(context, 'Veuillez vérifier vos coordonnées');
            }
          },
          child: BlocBuilder(
              bloc: _authBloc,
              builder: (c, AuthState state) {
                return body(c, state);
              }),
        ));
  }

  Widget body(BuildContext context, AuthState state){
    return SingleChildScrollView(

      child: Form(
          key: _formKey,
          child: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/register_bg.png"),
                      fit: BoxFit.fill
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 48),
                margin: EdgeInsets.symmetric(vertical: 84, horizontal: 32),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(6))
                ),
                child: Column(
                  children: [
                    Image.asset('assets/images/logo.png', height: 200),
                    SizedBox(height: 64),
                    emailField(),
                    SizedBox(height: 16),
                    passwordField(),
                    SizedBox(height: 16),
                    Row(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceAround,
                      children: [

                        GestureDetector(
                          onTap: (){

                          },
                          child: Container(
                            color: Colors.transparent,
                            height: 40,

                            child:  Text(
                              "Mot de passe oublié?",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: AppColor().goldColorBg
                              ),
                            ),
                          ),
                        ),

                        GestureDetector(
                          onTap: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) => RegisterScreen()));
                          },
                          child: Container(
                            color: Colors.transparent,
                            height: 40,
                            child: Text(
                              "S'inscrire",
                              style: TextStyle(
                                  fontSize: 16,
                                  color: AppColor().goldColorBg
                              ),
                            ),
                          ),

                        )

                      ],
                    ),
                    SizedBox(height: 16),
                    submitBtn()
                  ],
                ),
              ),
            ],
          )


      ),
    );
  }

  Widget emailField() {
    return Container(
        child: TextFormField(
          controller: emailController,
          cursorColor: Colors.black,
          keyboardType: TextInputType.emailAddress,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
          validator: validateEmail,
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Email', Colors.white.withAlpha(150), Icon(Icons.email, color: Colors.black)),
        ));
  }

  Widget passwordField() {
    return Container(
        child: TextFormField(
          controller: passwordController,
          cursorColor: Colors.black,
          obscureText: true,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black, letterSpacing: 2),
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Mot de passe', Colors.white.withAlpha(150), Icon(Icons.lock_rounded, color: Colors.black,)),
          validator: validateNotEmpty,
        ));
  }


  Widget submitBtn() {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 16),
        height: 38,
        width: 240,
        decoration: BoxDecoration(
            color: AppColor().goldColorBg,
            borderRadius: const BorderRadius.all(Radius.circular(6))),
        child: TextButton(
            onPressed: () {
              print("login");
              if (_formKey.currentState!.validate()) {
                //verify form data
                _formKey.currentState!.save();
                //all fields are good
                _authBloc.add(LoginUser(emailController.text, passwordController.text));
              }
            },
            child: Text('Connexion',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600))));
  }
}