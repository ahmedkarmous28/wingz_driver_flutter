import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wingz_driver/bloc/auth_bloc/auth_bloc.dart';
import 'package:wingz_driver/globals/colors.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/globals/decoration_tf.dart';
import 'package:wingz_driver/globals/mixins.dart';
import 'package:wingz_driver/screens/commands_screen/commands_screen.dart';
import 'package:wingz_driver/services/alert_service.dart';
import 'package:country_code_picker/country_code_picker.dart';


class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new RegisterScreenState();
  }
}

class RegisterScreenState extends State<RegisterScreen>  with ValidationMixin {

  final _formKey = GlobalKey<FormState>();
  final _authBloc = AuthBloc();
  bool isLoading = false;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  String countryCode = "+33";

  void initState() {
    super.initState();
    phoneController.text = "+33";
  }

  @override
  void dispose() {
    _authBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:  BlocListener(
          bloc: _authBloc,
          listener: (c, AuthState state) async {
            if (state is AuthInitial) {
              print("-------state-------");
              print(state);
              EasyLoading.show(status: 'WINGZ');
            }

            if (state is RegisterUserState) {
              print("Register success");
              EasyLoading.dismiss();

              final SharedPreferences prefs = await SharedPreferences.getInstance();
              if(state.response.token != null){
                prefs.setString(TOKEN, state.response.token!);
                prefs.setString(USER_ID, state.response.id.toString());
                prefs.setBool(CONNECTED, true);


                prefs.setString(NAME, nameController.text );
                prefs.setString(EMAIL, emailController.text );
                prefs.setString(PHONE, phoneController.text );

                Future.delayed(Duration(milliseconds: 1500), () {
                  EasyLoading.dismiss();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => CommandTabScreen()));
                });
              }else{
                AlertService().showSuccessAlert(context, 'Compte livreur créé. Veuillez attendre un appel de notre part pour activer votre compte.');
                Future.delayed(Duration(milliseconds: 1500), () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                });
              }

            }

            if (state is ErrorState ) {
              EasyLoading.dismiss();
              AlertService().showErrorAlert(context, 'Veuillez vérifier vos coordonnées');
            }
          },
          child: BlocBuilder(
              bloc: _authBloc,
              builder: (c, AuthState state) {
                return body(c, state);
              }),
        ));
  }

  Widget body(BuildContext context, AuthState state){
    return SingleChildScrollView(
      child: Form(
          key: _formKey,
          child:  Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/register_bg.png"),
                  fit: BoxFit.fill
              ),
            ),
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 48),
              margin: EdgeInsets.symmetric(vertical: 84, horizontal: 32),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(6))
              ),
              child: Column(
                children: [
                  Image.asset('assets/images/logo.png', height: 200),
                  SizedBox(height: 84),
                  nameField(),
                  SizedBox(height: 16),

                  Row(
                    children: [
                      SizedBox(
                        width: 100,
                        height: 60,
                        child: CountryCodePicker(
                          enabled: true,
                          onChanged: (c) {
                            phoneController.text = "${c.dialCode}";
                            countryCode = "${c.dialCode}";
                          },
                          initialSelection: 'FR',
                          showCountryOnly: true,
                          showOnlyCountryWhenClosed: true,
                          favorite: ['+33', 'FR'],
                        ),
                      ),phoneField(),
                    ],
                  ),


                  SizedBox(height: 16),
                  emailField(),
                  SizedBox(height: 16),
                  passwordField(),
                  SizedBox(height: 16),
                  submitBtn()
                ],
              ),
            ),
          )
      ),
    );
  }

  Widget nameField() {
    return Container(
        child: TextFormField(
          cursorColor: Colors.black,
          controller: nameController,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black, letterSpacing: 2),
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Nom - Prénom', Colors.white.withAlpha(150), Icon(Icons.person_rounded, color: Colors.black,)),
          validator: validateNotEmpty,
        ));
  }

  Widget phoneField() {
    return Container(
      width: MediaQuery.of(context).size.width - 200,
        child: TextFormField(
          cursorColor: Colors.black,
          keyboardType: TextInputType.phone,
          controller: phoneController,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black, letterSpacing: 2),
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Téléphone', Colors.white.withAlpha(150), Icon(Icons.phone, color: Colors.black,)),
          validator: validateNotEmpty,
        ));
  }

  Widget emailField() {
    return Container(
        child: TextFormField(
          cursorColor: Colors.black,
          controller: emailController,
          keyboardType: TextInputType.emailAddress,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black),
          validator: validateEmail,
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Email', Colors.white.withAlpha(150), Icon(Icons.email, color: Colors.black)),
        ));
  }

  Widget passwordField() {
    return Container(
        child: TextFormField(
          cursorColor: Colors.black,
          controller: passwordController,
          obscureText: true,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w400, color: Colors.black, letterSpacing: 2),
          decoration: DecorationTF().decorationCalTFWithPlaceholder('Mot de passe', Colors.white.withAlpha(150), Icon(Icons.lock_rounded, color: Colors.black,)),
          validator: validatePasswordNotEmpty,
        ));
  }


  Widget submitBtn() {
    return Container(
        margin: EdgeInsets.only(top: 8, bottom: 16),
        height: 38,
        width: 240,
        decoration: BoxDecoration(
            color: AppColor().goldColorBg,
            borderRadius: const BorderRadius.all(Radius.circular(6))),
        child: TextButton(
            onPressed: () {

              if (_formKey.currentState!.validate()) {
                //verify form data
                _formKey.currentState!.save();
                //all fields are good

                var data = {
                  "country_code" : countryCode,
                  "name": nameController.text,
                  "email": emailController.text,
                  "password": passwordController.text,
                  "phone": phoneController.text,
                  "app_secret": "app_secret",
                };

                print(data);
                return;
                _authBloc.add(RegisterUser(data));
              }
            },
            child: Text("S'inscrire",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    fontWeight: FontWeight.w600))));
  }
}