import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:wingz_driver/bloc/api_settings.dart';
import 'package:wingz_driver/bloc/command_bloc/command_bloc.dart';
import 'package:wingz_driver/globals/colors.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/models/earningz_model.dart';
import 'package:wingz_driver/models/get_notifications_model.dart';
import 'package:wingz_driver/screens/menu_screen/menu_screen.dart';
import 'package:wingz_driver/services/alert_service.dart';
import 'package:wingz_driver/services/date_service.dart';
import 'package:wingz_driver/services/ioc_container.dart';

class ProfitsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ProfitsScreenState();
  }
}

class ProfitsScreenState extends State<ProfitsScreen> {

  final GlobalKey<ScaffoldState> _key = GlobalKey(); // Create a key
  final _commandBloc = CommandBloc();
  bool isLoading = false;
  late Earning earning ;

  @override
  void initState() {
    super.initState();
    _commandBloc.add(GetEarnings());
  }

  @override
  void dispose() {
    _commandBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: Implement build
    return Scaffold(
        key: _key,
        drawer: Container(
            width: MediaQuery.of(context).size.width * 0.85,
            child: MenuScreen()
        ),
        appBar: AppBar(
          title:Text('PROFITS', style: TextStyle(fontSize: 20, color: Colors.black, fontWeight: FontWeight.bold)),
          centerTitle: false,
          backgroundColor: Colors.white,
          elevation: 0,
          leading:Row(
            children: [ IconButton(
              onPressed: () => _key.currentState!.openDrawer(),
              icon: Icon(Icons.menu_rounded, color: Colors.black,size: 35),
            )],
          ),
        ),
        backgroundColor: backgroundColor,

        body:  BlocListener(
          bloc: _commandBloc,
          listener: (c, CommandState state) async {
            if (state is CommandInitial) {
              print("-------state-------");
              print(state);
              EasyLoading.show(status: 'WINGZ');
            }

            if(state is SuccessEarningsState){
              EasyLoading.dismiss();
              print("loaded");
              earning = state.response;
              isLoading = true;
            }

            if (state is ErrorState ) {
              EasyLoading.dismiss();
              if(state.error != "error"){
                AlertService().showErrorAlert(context, 'Erreur Serveur, veuillez réessayer plus tard.');
              }
            }

          },
          child: BlocBuilder(
              bloc: _commandBloc,
              builder: (c, CommandState state) {
                return body(c, state);
              }),
        ));
  }

  Widget body(BuildContext context, CommandState state){
    return SingleChildScrollView(
      child: isLoading ? Column(
        children: [
           Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(6))
              ),
              child : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Aujourd'hui", style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 6),
                  Divider(),
                  SizedBox(height: 6),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Nombre courses",style: TextStyle(fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.bold)),
                      Text("${earning.today.orders}",style: TextStyle(fontSize: 16, color: AppColor().goldColorBg, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  SizedBox(height: 12),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Total Profits",style: TextStyle(fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.bold)),
                      Text("${earning.today.earning.toStringAsFixed(2)}€",style: TextStyle(fontSize: 16, color: Colors.redAccent , fontWeight: FontWeight.bold)),
                    ],
                  ),],
              )

          ) ,
          Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(6))
              ),
              child : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Cette semaine", style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 6),
                  Divider(),
                  SizedBox(height: 6),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Nombre courses",style: TextStyle(fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.bold)),
                      Text("${earning.week.orders}",style: TextStyle(fontSize: 16, color: AppColor().goldColorBg, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  SizedBox(height: 12),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Total Profits",style: TextStyle(fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.bold)),
                      Text("${earning.week.earning.toStringAsFixed(2)}€",style: TextStyle(fontSize: 16, color: Colors.redAccent, fontWeight: FontWeight.bold)),
                    ],
                  ),
                            ],
              )

          ) ,
          Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(6))
              ),
              child : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Ce mois", style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 6),
                  Divider(),
                  SizedBox(height: 6),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Nombre courses",style: TextStyle(fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.bold)),
                      Text("${earning.month.orders}",style: TextStyle(fontSize: 16, color: AppColor().goldColorBg, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  SizedBox(height: 12),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Total Profits",style: TextStyle(fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.bold)),
                      Text("${earning.month.earning.toStringAsFixed(2)}€",style: TextStyle(fontSize: 16, color: Colors.redAccent, fontWeight: FontWeight.bold)),
                    ],
                  ),
                        ],
              )

          ) ,
          Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
              margin: EdgeInsets.symmetric(vertical: 8, horizontal: 32),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(6))
              ),
              child : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Anciens", style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.bold)),
                  SizedBox(height: 6),
                  Divider(),
                  SizedBox(height: 6),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Nombre courses",style: TextStyle(fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.bold)),
                      Text("${earning.previous.orders}",style: TextStyle(fontSize: 16, color: AppColor().goldColorBg, fontWeight: FontWeight.bold)),
                    ],
                  ),
                  SizedBox(height: 12),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Total Profits",style: TextStyle(fontSize: 16, color: Colors.blueGrey, fontWeight: FontWeight.bold)),
                      Text("${earning.previous.earning.toStringAsFixed(2)}€",style: TextStyle(fontSize: 16, color: Colors.redAccent, fontWeight: FontWeight.bold)),
                    ],
                  ),
                              ],
              )

          ) ,
        ],
      ) : SizedBox()
    );
  }
}