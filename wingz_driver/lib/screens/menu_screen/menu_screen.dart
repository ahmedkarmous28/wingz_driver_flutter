import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wingz_driver/globals/colors.dart';
import 'package:wingz_driver/globals/constants.dart';
import 'package:wingz_driver/screens/commands_screen/commands_screen.dart';
import 'package:wingz_driver/screens/login_screen/login_screen.dart';
import 'package:wingz_driver/screens/notification_screen/notification_screen.dart';
import 'package:wingz_driver/screens/profile_screen/profile_screen.dart';
import 'package:wingz_driver/screens/profits_screen/profits_screen.dart';

class MenuScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new MenuScreenState();
  }
}

class MenuScreenState extends State<MenuScreen>   {


  @override
  Widget build(BuildContext context) {
    // TODO: Implement build

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 32),
      color: AppColor().registerBgBlack,
      child: Column(
        children: [
          SizedBox(height: 48),
          Center(
              child: Image.asset('assets/images/logo.png', height: 240)
          ),
          Container(
            height: 300,
            child: ListView(
              children: [
                ListTile(
                    title: Text('Commandes', style: TextStyle(fontSize: 18, color: Colors.grey)),
                    leading: Icon(Icons.list, size: 30, color: AppColor().goldColorBg),
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => CommandTabScreen()));

                    }
                ),
                ListTile(
                    title: Text('Revenus', style: TextStyle(fontSize: 18, color: Colors.grey)),
                    leading: Icon(Icons.euro, size: 30, color: AppColor().goldColorBg),
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => ProfitsScreen()));

                    }
                ),
                ListTile(
                    title: Text('Notifications', style: TextStyle(fontSize: 18, color: Colors.grey)),
                    leading: Icon(Icons.add_alert_rounded, size: 30, color: AppColor().goldColorBg),
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => NotificationsScreen()));

                    }
                ),
                ListTile(
                    title: Text('Profil', style: TextStyle(fontSize: 18, color: Colors.grey)),
                    leading: Icon(Icons.person_rounded, size: 30, color: AppColor().goldColorBg),
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) => ProfileTabScreen()));

                    }
                ),
              ],
            )
          )
        ],
      ),
    );
  }

}