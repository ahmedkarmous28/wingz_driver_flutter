import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:wingz_driver/screens/splash_screen.dart';
import 'package:wingz_driver/services/ioc_container.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'globals/constants.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setupIocContainer();

  ///OneSignal
  _setupOneFignalPushNotifications();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        backgroundColor: backgroundColor,
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
        builder: EasyLoading.init()
    );
  }
}


_setupOneFignalPushNotifications(){
  //Remove this method to stop OneSignal Debugging
  OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);

  OneSignal.shared.setAppId(onesignalid);

// The promptForPushNotificationsWithUserResponse function will show the iOS push notification prompt. We recommend removing the following code and instead using an In-App Message to prompt for notification permission
  OneSignal.shared.promptUserForPushNotificationPermission().then((accepted) {
    print("Accepted permission: $accepted");
  });
}